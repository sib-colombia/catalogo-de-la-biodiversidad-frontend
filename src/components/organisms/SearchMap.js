/* eslint-disable react/prop-types */
import React, { Component } from 'react';

import HumboldtMap from '../molecules/HumboldtMap';
import { TileLayer, LayersControl } from 'react-leaflet';

class SearchMap extends Component {


  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const b = this.props.busqueda.substring(1);
    return (<HumboldtMap tipo="occurrences" center={[4.36, -74.04]} zoom={6} >
      <LayersControl.Overlay name='Registros' checked readonly>
        <TileLayer
          url={'https://api.gbif.org/v2/map/occurrence/density/{z}/{x}/{y}@2x.png?style=fire.point&publishingCountry=CO&'+b}
          attribution='&copy; GBIF'
        />
      </LayersControl.Overlay>
    </HumboldtMap>
    );
  }
}

export default SearchMap;
