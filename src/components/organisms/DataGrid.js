/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { map, findIndex, compact, flattenDeep } from 'lodash';

import Data from '../molecules/Data';

class DataGrid extends Component {
  render() {
    const { data, grid } = this.props;


    const ignorar = ['_id', 'ancillaryData'];
    let lista = map(data, (d, k) => {
      if ((findIndex(ignorar, (i) => {return i === k;})) === -1) return k;
    });
    lista = compact(flattenDeep(lista));


    return (<div className={`uk-child-width-${grid} uk-grid-small`} data-uk-grid="data-uk-grid">
      {map(lista, (contact, key) => (<Data key={key} data={contact} />))}
    </div>);
  }
}

export default DataGrid;
