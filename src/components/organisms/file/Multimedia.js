/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import ReactAudioPlayer from 'react-audio-player';

import { map } from 'lodash';
import DataSheet from '../../molecules/DataSheet';
import Galeria from '../../molecules/Galeria';

class Multimedia extends Component {

  getType(v){
    if(v.source){
      const f = v.source.toLowerCase().split('.');
      //console.log("tipo: ", v);
      //console.log(f);
      if (
        f[f.length-1]==='jpg' ||
        f[f.length-1]==='jpeg' ||
        f[f.length-1]==='png' ||
        f[f.length-1]==='bmp' ||
        f[f.length-1]==='gif'
      ){
        //console.log("Es una imágen")
        return 'image';
      }
      if (
        f[f.length-1]==='mp3' ||
        f[f.length-1]==='m4a' ||
        f[f.length-1]==='aac' ||
        f[f.length-1]==='ogg' ||
        f[f.length-1]==='wav'
      ){
        // console.log("Es un sonido")
        return 'sound';
      }
      if (
        f[f.length-1]==='ogg' ||
        f[f.length-1]==='mp4'
      ){
        // console.log("Es un video")
        return 'video';
      }
    }
    return null;
  }

  getSonidos(d){
    if (d.ancillaryDataApprovedInUse && d.ancillaryDataApprovedInUse.ancillaryData){
    
      let s = map(d.ancillaryDataApprovedInUse.ancillaryData, (v, k)=> {
        if (this.getType(v)==='sound' && v.mediaURL[0]){
          return true
        }
      })
      if (s === "")

        return (
          <DataSheet title="Sonidos" scroll="sounds">
            <div className="uk-padding-small uk-grid-small uk-child-width-1-1" data-uk-grid="">
              { d.ancillaryDataApprovedInUse.ancillaryData && (map(d.ancillaryDataApprovedInUse.ancillaryData, (v, k)=> {
                // console.log("Analizando")
                // console.log(v)
                if (this.getType(v)==='sound' && v.mediaURL[0]){
                  // console.log("Imprimiendo una imágen de ",v.mediaURL)
                  return (
                    <div>
                      <span className="uk-h5 uk-margin-small-right">{v.rightsHolder+' / '+v.license+' / '+v.source}</span>
                      <ReactAudioPlayer
                        src={v.mediaURL[0]}
                        controls
                        className="uk-audio"
                      />
                    </div>);
                }
              }))}
            </div>
          </DataSheet>
        );
      }
    return null;
  }

  getOtros(d){
    if (d.ancillaryDataApprovedInUse && d.ancillaryDataApprovedInUse.ancillaryData){
      let s = map(d.ancillaryDataApprovedInUse.ancillaryData, (v, k)=> {
        if (this.getType(v)!=='image' && this.getType(v)!=='sound' && v.mediaURL[0]){
          return true
        }
      })
      if (s === "")
      return (
        <DataSheet title="Otros" scroll="others">
          <div className="uk-padding-small uk-grid-small uk-child-width-1-2" data-uk-grid="">
            { d.ancillaryDataApprovedInUse.ancillaryData && (map(d.ancillaryDataApprovedInUse.ancillaryData, (v, k)=> {
              // console.log("Analizando")
              // console.log(v)
              if (this.getType(v)!=='image' && this.getType(v)!=='sound' && v.mediaURL[0]){
                // console.log("Imprimiendo un otro de ",v.mediaURL)
                return (
                  <a key={k} className="uk-text-tertiary" href="">https://opensource.org/licenses/MIT</a>
                );
              }
            }))}
          </div>
        </DataSheet>
      );
    }

  }


  render() {
    // console.log("Desde multimedia")
    // console.log(this.props.data)
    return (
      <div className="uk-container uk-width-5-6@l uk-width-2-3@xl uk-margin-top">
        <div className="uk-grid-small" data-uk-grid="">
          <div className="uk-width-1-3">
            <div className="uk-card uk-card-default" data-uk-sticky="offset: 90; bottom: true" style={{ zIndex: 979 }}>
              <div className="uk-card-header">
                <h4>Multimedia</h4>
              </div>
              <div className="uk-padding-small">
                <ul className="uk-list uk-list-large uk-margin-remove-bottom" uk-scrollspy-nav="closest: li; cls: uk-text-primary; offset: 90;">
                  <li><a className="uk-link-reset" href="#images" data-uk-scroll="offset: 90">Imágenes</a></li>
                  { this.props.data && this.getSonidos(this.props.data) && <li><a className="uk-link-reset" href="#sounds" data-uk-scroll="offset: 90">Sonidos</a></li> }
                  { this.props.data && this.getOtros(this.props.data) && <li><a className="uk-link-reset" href="#others" data-uk-scroll="offset: 90">Otros</a></li> }
                </ul>
              </div>
            </div>
          </div>
          <div className="uk-width-expand">
            <Galeria data={this.props.data} />
            { this.props.data && this.getSonidos(this.props.data) }
            { this.props.data && this.getOtros(this.props.data) }
          </div>
        </div>
      </div>
    );
  }
}

export default Multimedia;
