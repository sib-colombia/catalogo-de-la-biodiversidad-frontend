import React, { Component } from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const RangeComponent = Slider.Range;

class Range extends Component {

  constructor() {
    super();
    this.state = {
      value: [3000, 6000],
    };
  }

  onSliderChange = (value) => {
    this.setState({
      value,
    });
  }

  render() {
    return (
      <RangeComponent
        step={100}
        allowCross={false}
        value={this.state.value}
        onChange={this.onSliderChange}
        min={0} max={10000}
        trackStyle={[{ backgroundColor: '#666', height: 1.5 }]}
        handleStyle={[{ backgroundColor: '#666', border: 0, width: 10, height: 10 }, { backgroundColor: '#666', border: 0, width: 10, height: 10 }]}
        railStyle={{ height: 1.5 }}
      />
    );
  }
}

export default Range;
