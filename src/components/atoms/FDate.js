/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-duplicate-props */
import React, { Component } from 'react';

class FDate extends Component {

  constructor(props) {
    super(props);
    this.updateValue = this.updateValue.bind(this);
  }

  updateValue(event) {
    this.props.onDataChange({ path: this.props.path, value: event.target.value });
  }

  render() {
    return (
      <div>
        <h5>{this.props.value.title}</h5>
        <input type="text" data-uk-datepicker data-uk-datepicker="{format:'DD-MM-YYYY'}" className="uk-input" onKeyUp={this.updateValue} />
      </div>
    );
  }
}

export default FDate;
