/* eslint-disable react/prop-types */
import React, { Component } from 'react';

class Reference extends Component {

  render() {
    return (
      <div id="reference" className="uk-flex uk-flex-center uk-flex-middle uk-text-bold uk-background-primary uk-grid-collapse uk-child-width-1-1 uk-child-width-auto@s uk-width-1" data-uk-grid>
        <div className="uk-text-default" style={{ paddingBottom: 5, paddingLeft: 5, paddingRight: 4 }} >
          <span uk-icon="icon: star"></span>
        </div>
        <div className={`${!this.props.img ? 'uk-background-default' : ''} uk-text-center uk-width-expand`} style={{ border: 'solid 2px #ff7847', padding: 1.5 }}>
          <span className={`uk-text-small uk-text-break ${this.props.img && 'uk-text-default'}`}>Lista de referencia</span>
        </div>
      </div>
    );
  }
}

export default Reference;
