import React, { Component } from 'react';

class UICNSlotMini extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    let { name, id, select, containerBase, content, tooltip} = this.props
    if (content === undefined) content=true
    if (tooltip === undefined) tooltip=''
    let container = ''
    if (containerBase === undefined){
      container = 'uk-text-center uk-text-middle content-caja'
    }else{
      container = containerBase
    }
    
    //let className = 'uk-text-center uk-text-middle UICNSlotContainer '
    let className = container
    let classNameCaja = ''
    if (select===id){
      //className += 'uk-width-1-5'
      classNameCaja = 'UICN_'+id
    }else{
      //className += 'uk-width-1-10'
    }
    let t = undefined
    if (tooltip) t = "Categoría UICN "+tooltip.toLowerCase()
    return (
      <span className={className}>
        <div className={"uk-panel caja cajaMini caja-"+classNameCaja} uk-tooltip={t}>
          <div className={"title "}>{id}</div>
          {content && <div className={"content "}>{name}</div>}
        </div>
      </span>
    );
  }
}

export default UICNSlotMini;
