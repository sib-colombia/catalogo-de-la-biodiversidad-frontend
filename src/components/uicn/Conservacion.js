import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

/* Style Components */
import { Paragraph } from '../../pages/styled';

const Conservacion = ({ data }) => {
  const { conservation } = data;
  const { actions } = conservation;

  return (
    <div className="uk-margin">
      <div id={'conservacion'} className={'uk-card uk-card-default uk-panel uk-padding uk-text-break'}>
        <div className="uk-flex uk-flex-column">
          <span className={'uk-h3 uk-text-tertiary uk-text-bold'}>{'Conservación'}</span>
        </div>

        <div className="uk-padding-small type_hierarchy">
          <div className="uk-child-width-1-1">
            <li>
              <div className="uk-child-width-1-2 uk-grid" data-uk-grid="">
                <span className="uk-text-bold uk-first-column">Acciones de conservación:</span>
              </div>
            </li>
            <Paragraph>{actions}</Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

Conservacion.propTypes = {
  data: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  data: state.get('iucnReducer').get('data'),
});


export default connect(
  mapStateToProps,
  null
)(withRouter(Conservacion));
