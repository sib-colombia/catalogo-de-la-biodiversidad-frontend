import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

/* Style Components */
import { Paragraph } from '../../pages/styled';

const Habitat = ({ data }) => {
  const { habitat } = data;
  const { documentation, system } = habitat;

  return (
    <div className="uk-margin">
      <div id={'habitat'} className={'uk-card uk-card-default uk-panel uk-padding uk-text-break'}>
        <div className="uk-flex uk-flex-column">
          <span className={'uk-h3 uk-text-tertiary uk-text-bold'}>{'Hábitat y ecología'}</span>
        </div>

        <div className="uk-padding-small type_hierarchy">
          <div className="uk-child-width-1-1">
            <li>
              <div className="uk-child-width-1-2 uk-grid" data-uk-grid="">
                <span className="uk-text-bold uk-first-column">Sistema:</span>
                <span>{system}</span>
              </div>
            </li>
            <br />
            <li>
              <div className="uk-child-width-1-2 uk-grid" data-uk-grid="">
                <span className="uk-text-bold uk-first-column">Hábitat:</span>
              </div>
            </li>
            <Paragraph>{documentation}</Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

Habitat.propTypes = {
  data: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  data: state.get('iucnReducer').get('data'),
});


export default connect(
  mapStateToProps,
  null
)(withRouter(Habitat));
