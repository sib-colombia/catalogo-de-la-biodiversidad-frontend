/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { map, findIndex, compact, flattenDeep } from 'lodash';
import { T } from '../../services/UtilService';

class Data extends Component {

  campo(id){
    const { data } = this.props;
    const d = data[id];
    if (data && d!==null && d!==undefined && d!==''){
      return d;
    }
    return null;
  }

  render() {
    const { data } = this.props;
    const ignorar = ['_id', 'ancillaryData'];
    let lista = map(data, (d, k) => {
      if ((findIndex(ignorar, (i) => {return i === k;})) === -1) return k;
    });
    lista = compact(flattenDeep(lista));
    return (<div>
      { lista.length > 0 &&
        <ul className="uk-list uk-list-divider">
          {map(lista, (value, key) => {
            if (this.campo(value)!==null)
              return (<li key={key}><div className="uk-child-width-1-2" data-uk-grid="">
                <span className="uk-text-bold">{T(value)}</span><span>{this.campo(value)}</span>
              </div></li>);
          })}
        </ul>
      }

    </div>);
  }
}

export default Data;
