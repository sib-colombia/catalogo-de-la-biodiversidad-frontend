import React, { Component } from 'react';

import Filters from '../Filters';

class License extends Component {
  render() {
    return (
      <Filters.Base title="Licencia">
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" type="checkbox" />CC 1.0 <span className="uk-float-right">325.000</span></label>
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" type="checkbox" />CC BY 4.0  <span className="uk-float-right">175.000</span></label>
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" type="checkbox" />CC BY-NC 4.0 <span className="uk-float-right">65.000</span></label>
        </div>
      </Filters.Base>
    );
  }
}

export default License;