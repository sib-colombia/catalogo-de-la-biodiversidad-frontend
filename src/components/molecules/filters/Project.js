import React, { Component } from 'react';

import Filters from '../Filters';

class Project extends Component {
  render() {
    return (
      <Filters.Base title="Proyecto">
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" defaultChecked type="checkbox" />Colombia BIO<span className="uk-float-right">8.523</span></label>
        </div>
      </Filters.Base>
    );
  }
}

export default Project;