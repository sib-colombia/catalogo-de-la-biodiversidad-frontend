import React, { Component } from 'react';

import Filters from '../Filters';

class TaxonID extends Component {
  render() {
    return (
      <Filters.Base title="Número de catálogo">
        <input className="uk-input uk-form-small" type="text" placeholder="Escribe el nombre del catálogo" />
      </Filters.Base>
    );
  }
}

export default TaxonID;