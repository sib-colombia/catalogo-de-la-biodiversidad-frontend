import React, { Component } from 'react';

import Filters from '../Filters';

class EventDate extends Component {
  render() {
    return (
      <Filters.Base title="Fecha de publicación">
        <div>
          <div uk-form-custom="target: > * > span:first-child">
            <select className="uk-select">
              <option value="1">Es</option>
              <option value="2">Máxima</option>
              <option value="3">Mínima</option>
              <option value="4">Entre</option>
            </select>
            <button className="uk-button uk-button-text" tabIndex="-1">
              <span></span>
              <span uk-icon="icon: triangle-down; ratio: 0.8"></span>
            </button>
          </div>
        </div>
        <form className="uk-grid-small uk-child-width-expand" data-uk-grid="">
          <div className="uk-width-1-3">
            <input className="uk-input" type="text" placeholder="11" />
          </div>
          <div>
            <select className="uk-select">
              <option>Mes</option>
            </select>
          </div>
        </form>
        <div className="uk-text-center">Y</div>
        <form className="uk-grid-small uk-child-width-expand" data-uk-grid="">
          <div className="uk-width-1-3">
            <input className="uk-input" type="text" placeholder="11" />
          </div>
          <div>
            <select className="uk-select">
              <option>Mes</option>
            </select>
          </div>
        </form>
        <hr />
        <div className="uk-flex-between" data-uk-grid="">
          <div><button className="uk-button uk-button-default uk-button-small">Limpiar</button></div>
          <div><button className="uk-button uk-button-primary uk-button-small">Añadir</button></div>
        </div>
      </Filters.Base>
    );
  }
}

export default EventDate;
