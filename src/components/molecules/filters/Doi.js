import React, { Component } from 'react';

import Filters from '../Filters';

class Doi extends Component {
  render() {
    return (
      <Filters.Base title="DOI">
        <input className="uk-input uk-form-small" type="text" placeholder="Escribe el DOI" />
      </Filters.Base>
    );
  }
}

export default Doi;