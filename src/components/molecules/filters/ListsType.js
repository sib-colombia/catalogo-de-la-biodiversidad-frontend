import React, { Component } from 'react';

import Filters from '../Filters';

class ListsType extends Component {
  render() {
    return (
      <Filters.Base title="Tipo de colección">
        <input className="uk-input uk-form-small" type="text" placeholder="Escribe el tipo de colección" />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" type="checkbox" />Tipo de colección 1<span className="uk-float-right">325.000</span></label>
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" type="checkbox" />Tipo de colección 2 <span className="uk-float-right">175.000</span></label>
          <label className="uk-form-small"><input className="uk-checkbox uk-margin-small-right" type="checkbox" />Tipo de colección 3 <span className="uk-float-right">65.000</span></label>
        </div>
      </Filters.Base>
    );
  }
}

export default ListsType;