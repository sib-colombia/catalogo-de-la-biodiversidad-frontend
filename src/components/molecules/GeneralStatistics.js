/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import map from 'lodash/map';

class GeneralStatistics extends Component {
  render() {
    return (
      <div className="uk-card uk-card-default uk-padding-small uk-card-body">
        <div className={!this.props.home ? 'uk-container uk-width-5-6@m uk-width-2-3@l' : ''}>
          <div className="uk-grid-divider uk-child-width-expand@s uk-text-center uk-grid-small" data-uk-grid>
            {
              map(this.props.data, (value, key) => (
                <div key={key}>
                  <Link
                    to=""
                    className="uk-link-reset"
                  >
                    <span className="uk-h2 uk-margin-small-right">
                      <NumberFormat
                        value={value}
                        displayType="text"
                        thousandSeparator
                      />
                    </span>
                    {key}
                  </Link>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default GeneralStatistics;
