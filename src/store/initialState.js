// const INITIAL_STATE = {
//   isLogged: false,
//   loading: false,
//   error: null,
//   iucnSelected: null,
//   filters: {
//     query: '',
//     taxonomia: {
//       reino: [],
//       grupoAnimales: [],
//       grupoPlantas: []
//     },
//     departamento: [],
//     tematica: {
//       especiesAmenazadas: [],
//       cites: [],
//       invasoras: []
//     },
//     multimedia: []
//   },
//   loginForm: {
//     username: '',
//     password: ''
//   },
//   userForm: {
//     username: '',
//     password: '',
//     email: '',
//     name: '',
//     lastname: '',
//     photo: '',
//     provider: '',
//     roles: [],
//     status: 0,
//     date: '',
//     last_login: '',
//     social: null
//   },
//   permissionForm: {
//     _id: null,
//     name: '',
//     description: '',
//     url: '',
//     active: false,
//     actions: [],
//     system: ''
//   },
//   rolForm: {
//     _id: null,
//     name: '',
//     description: '',
//     active: false,
//     permissions: [],
//     system: ''
//   },
//   iucnForm: {
//     ident: {
//       CBC: null,
//       IUCN: null
//     },
//     assessment: {
//       category: null,
//       criteria: null,
//       date: '',
//       language: '',
//       assesor: '',
//       reviewer: '',
//       contributor: '',
//       facilitator: '',
//       scope: '',
//       justification: ''
//     },
//     distribution: {
//       country: '',
//       range: '',
//       elevationLower: 0,
//       elevationUpper: 0,
//       eoo: 0,
//       aoo: 0,
//       locationsNumber: 0
//     },
//     population: {
//       justification: '',
//       severeFragmentation: '',
//       subpopulation: ''
//     },
//     threats: {
//       documentation: ''
//     },
//     habitat: {
//       documentation: '',
//       system: ''
//     },
//     conservation: {
//       actions: ''
//     },
//     use: {
//       documentation: ''
//     },
//     url: {
//       iucn: ''
//     },
//     _id: '',
//     __v: 0
//   }
// }

// export default INITIAL_STATE
