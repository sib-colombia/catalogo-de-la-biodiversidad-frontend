/* eslint-disable no-unused-vars */
import { URL_NEW_API_DEV, BASE_URL_USER } from '../config/const';
import endpoints from '../config/endpoints';

const wait = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve();
  }, 3000);
});

const fetchParams = (method, data = '') => {
  const body = data ? { body: JSON.stringify(data) } : {};

  return {
    method,
    headers: apiHeaders,
    credentials: 'same-origin',
    ...body,
  };
};

let apiHeaders = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
  Authorization: '',
};

export const apiUser = {
  getUser: async ({ token }) => {
    apiHeaders = { ...apiHeaders, Authorization: token };
    try {
      const response = await fetch(`${URL_NEW_API_DEV}${endpoints.user.me}`, fetchParams('GET'));
      if (!response.ok) {
        throw new Error(response.status_message);
      }
      const data = await response.json();
      if (data.error) {
        throw new Error(data.error);
      }
      return data;
    } catch (error) {
      console.error(error);
    }
  },
  createUser: async (user, token) => {
    apiHeaders = { ...apiHeaders, Authorization: token };
    try {
      const response = await fetch(`${URL_NEW_API_DEV}${endpoints.user.create}`, fetchParams('POST', { ...user }));
      if (!response.ok) {
        throw new Error(response.status_message);
      }
      const data = await response.json();
      if (data.error) {
        throw new Error(data.error);
      }
      return data;
    } catch (error) {
      console.error(error);
    }
  },
  getUsers: async ({ token, page, quantity }) => {
    apiHeaders = { ...apiHeaders, Authorization: token };
    try {
      const response = await fetch(`${URL_NEW_API_DEV}${endpoints.user.getUsers}/${page}/${quantity}`, fetchParams('GET'));
      if (!response.ok) {
        throw new Error(response.status_message);
      }
      const data = await response.json();
      if (data.error) {
        throw new Error(data.error);
      }
      return data;
    } catch (error) {
      console.error(error);
    }
  },
};
