export const FETCHING_ALBUM_REQUEST = '[ALBUM] request';
export const FETCHING_ALBUM_SUCCESS = '[ALBUM] success';
export const FETCHING_ALBUM_FAILURE = '[ALBUM] failure';
export const SET_ALBUM_FORM_CHANGE = '[ALBUM] SET_ALBUM_FORM_CHANGE';
