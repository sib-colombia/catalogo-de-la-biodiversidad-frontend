/* eslint-disable no-unused-vars */
import * as AlbumActions from '../actions';
import * as AlbumServices from '../newServices';

export const albumRequest = () => {
  return async (dispatch, getState) => {
    const state = getState();
    const { id } = state.get('albumReducer').get('albumForm');
    try {
      dispatch(AlbumActions.fetchingAlbumRequest());
      const data = await AlbumServices.apiIucn.cbc(id);
      dispatch(AlbumActions.fetchingAlbumSuccess(data));
    } catch (error) {
      dispatch(AlbumActions.fetchingAlbumFailure(error.message));
    } finally {
      console.log('albumRequest finally', state.get('albumReducer'));
    }
  };
};

export const albumFormChange = (name, value) => {
  return async (dispatch, getState) => {
    try {
      dispatch(AlbumActions.setAlbumFormChange(name, value));
    } catch (error) {
      console.log(error.message);
    }
  };
};
