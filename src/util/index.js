export function route(data, route) {
  let temp = data;
  for (let i = 0; i < route.length; i++) {
    temp = temp[route[i]];
    if (temp === undefined) {
      return '';
    }
  }
  return temp;
}

export function request(xhr, method, url) {
  return new Promise((resolve, reject) => {

    if (xhr) {
      xhr.abort();
    }

    xhr.open(method, url);
    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(JSON.parse(xhr.response));
      } else {
        reject({
          status: xhr.status,
          statusText: xhr.statusText,
        });
      }
    };

    xhr.onerror = () => {
      reject({
        status: xhr.status,
        statusText: xhr.statusText,
      });
    };

    xhr.send();
  });
}
