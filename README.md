# Catálogo de la Biodiversidad

Frontend del Portal de información de Catálogo de la Biodiversidad del SiB Colombia

## Requerimientos

- [Nodejs](https://nodejs.org) >= **12.x.x**

## Instalación

**npm**

```bash
git clone https://gitlab.com/sib-colombia/catalogo-de-la-biodiversidad-frontend.git
cd catalogo-de-la-biodiversidad-frontend
npm install
```

## Desarrollo

### Start

**Comando:** `npm start` 

**Descripción:** Inicia el proyecto en modo desarrollo

## Producción

Para el despliegue en producción se recomienda seguir estas [instrucciones](https://gitlab.com/sib-colombia/documentacion-productos-y-servicios/-/blob/master/Aplicaciones%20del%20SiB%20Colombia/Despliegue%20de%20aplicaciones%20frontend%20de%20los%20portales.md?ref_type=heads).

Nota: Para primer despliegue seguir todos los pasos y si es sólo actualización omitir el último ya que el servicio en pm2 ya estaría corriendo.

## Creación de la base de datos del catálogo

El proceso de restauración de la base de datos del catálogo a partir de la base de datos del editor de fichas tiene los siguientes pasos:

- Generar una copia de seguridad de la base de datos de Mamut mongodump --archive="chigui" --db=catalogoDbTest --username admin --password XXXXXXXXXX --authenticationDatabase admin

- Crear en la base de datos mongo una base de datos con el nombre que se quiera ejemplo: catalogo_YYYY-MM-DD

- El primer paso es con una copia de seguridad mongorestore --username catalogo --password XXXXXXXXXXXX --authenticationDatabase admin --db catalogo_YYYY-MM-DD catalogo_YYYY-MM-DD

- Entrar en el servidor a la carpeta de scripts de migración cd /opt/apps/catalogo-de-la-biodiversidad-backend/api/scripts

- Ejecutar el script de construcción de la colección Record node --max-old-space-size=16000 scriptElementsToRecord.js

- Ejecutar el script de migración de fichas de IUCN node --max-old-space-size=16000 newRecordsScript.js



Nota: la ejecución de los scripts tiene un comando para aumentar la memoria que puede usar javascript en la ejecución.


